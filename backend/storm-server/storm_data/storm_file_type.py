"""
Project: storm_data
Author: Saj Arora
Description: 
"""
import datetime

import re


def convert_to_datetime(value, year):
    try:
        date = datetime.datetime.strptime(value.lower(), '%d-%b-%y %H:%M:%S')
        if len(year) < 4:
            year = '19' + year
        return date.replace(year=int(year))
    except:
        raise Exception(value)

def convert_to_float(value):
    try:
        return float(re.sub("[^0-9|.]", "", value))
    except:
        return 0.0


def convert_to_int(value):
    try:
        return int(re.sub("[^0-9]", "", value))
    except:
        return 0

class StormFileType(object):
    DETAILS = 'storm_details'
    FATALATIES = 'storm_fatalaties'
    LOCATIONS = 'storm_locations'

    data_dict = {
        DETAILS: {
            'start_datetime': lambda x: convert_to_datetime(x.get('BEGIN_DATE_TIME'),
                                                            x.get('YEAR')),
            'end_datetime': lambda x: convert_to_datetime(x.get('END_DATE_TIME'),
                                                            x.get('YEAR')),
            'timezone': 'CZ_TIMEZONE',
            'id': lambda x: int(x.get('EVENT_ID')),
            'state': 'STATE',
            'state_fips': 'STATE_FIPS',
            'name': 'CZ_NAME',
            'type': 'EVENT_TYPE',
            'type_code': 'CZ_TYPE',
            'fips': 'CZ_FIPS',
            'direct_injuries': lambda x: convert_to_int(x.get('INJURIES_DIRECT')),
            'indirect_injuries': lambda x: convert_to_int(x.get('INJURIES_INDIRECT')),
            'direct_deaths': lambda x: convert_to_int(x.get('DEATHS_DIRECT')),
            'indirect_deaths': lambda x: convert_to_int(x.get('DEATHS_INDIRECT')),
            'property_damage': lambda x: convert_to_float(x.get('DAMAGE_PROPERTY')),
            'crops_damage': lambda x: convert_to_float(x.get('DAMAGE_CROPS')),
            'magnitude': lambda x: convert_to_float(x.get('MAGNITUDE')),
            'magnitude_type': 'MAGNITUDE_TYPE',
            'tor_f_scale': 'TOR_F_SCALE',
            'tor_length': 'TOR_LENGTH',
            'tor_width': 'TOR_WIDTH',
            'latitude': 'BEGIN_LAT',
            'longitude': 'BEGIN_LON',
            'description': 'EPISODE_NARRATIVE'
        },
        # FATALATIES: [
        #     'FATALITY_ID',
        #     'EVENT_ID',
        #     'FATALITY_TYPE',
        #     'FATALITY_AGE',
        #     'FATALITY_SEX',
        #     'FATALITY_LOCATION'
        # ],
        # LOCATIONS: []
    }

    data_csv_headers = {
        DETAILS: [
            'BEGIN_YEARMONTH',
            'BEGIN_DAY',
            'BEGIN_TIME',
            'END_YEARMONTH',
            'END_DAY',
            'END_TIME',
            'EPISODE_ID',
            'EVENT_ID',
            'STATE',
            'STATE_FIPS',
            'YEAR',
            'MONTH_NAME',
            'EVENT_TYPE',
            'CZ_TYPE',
            'CZ_FIPS',
            'CZ_NAME',
            'WFO',
            'BEGIN_DATE_TIME',
            'CZ_TIMEZONE',
            'END_DATE_TIME',
            'INJURIES_DIRECT',
            'INJURIES_INDIRECT',
            'DEATHS_DIRECT',
            'DEATHS_INDIRECT',
            'DAMAGE_PROPERTY',
            'DAMAGE_CROPS',
            'SOURCE',
            'MAGNITUDE',
            'MAGNITUDE_TYPE',
            'FLOOD_CAUSE',
            'CATEGORY',
            'TOR_F_SCALE',
            'TOR_LENGTH',
            'TOR_WIDTH',
            'TOR_OTHER_WFO',
            'TOR_OTHER_CZ_STATE',
            'TOR_OTHER_CZ_FIPS',
            'TOR_OTHER_CZ_NAME',
            'BEGIN_RANGE',
            'BEGIN_AZIMUTH',
            'BEGIN_LOCATION',
            'END_RANGE',
            'END_AZIMUTH',
            'END_LOCATION',
            'BEGIN_LAT',
            'BEGIN_LON',
            'END_LAT',
            'END_LON',
            'EPISODE_NARRATIVE',
            'EVENT_NARRATIVE',
            'DATA_SOURCE'
        ]
    }
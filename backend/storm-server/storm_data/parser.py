"""
Project: storm_data
Author: Saj Arora
Description: 
"""
import csv
import logging
import os

from google.appengine.ext import ndb

from api.v1 import SageResource, SageDateTime, SageString, SageInteger, SageFloat, SageText
from csv_reader import CsvReader
from storm_data.storm_file_type import StormFileType

data_dir = os.path.join(os.path.dirname(__file__), 'data/final')


def load_all_csv_in_dir(file_type, year):
    directory = data_dir + '/' + file_type
    all_data = []
    reader = CsvReader()

    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.find("d%s" % year) > 0 and file.endswith(".csv"):
                file_path = directory + '/' + file
                # instantiate csv reader class
                all_data = reader.get_data(file_path, file_type)
                logging.info("File parsed: %s" % file)
    return all_data


def parse_storm_details(module, year):
    module = module()
    module._build_model(module._build_validator())
    _MODEL = module.get_model()
    all_data = load_all_csv_in_dir(StormFileType.DETAILS, year)
    to_put = []
    for entity in all_data:
        entity_key = ndb.Key(_MODEL._get_kind(), entity.get('id'))
        entity_db = entity_key.get()

        if not entity_db:
            entity_db = _MODEL(**entity)
        else:
            entity_db.populate(**entity)
        to_put.append(entity_db)

    ndb.put_multi(to_put)

"""
Project: storm_data
Author: Saj Arora
Description: Structure for storm data
"""

class Storm(object):
    timestamp = None
    'BEGIN_DATE_TIME',
                'END_DATE_TIME',
                'CZ_TIMEZONE',
                'EVENT_ID',
                'STATE',
                'STATE_FIPS',
                'CZ_NAME',
                'EVENT_TYPE',
                'CZ_TYPE',
                'CZ_FIPS',
                'INJURIES_DIRECT',
                'INJURIES_INDIRECT',
                'DEATHS_DIRECT',
                'DEATHS_INDIRECT',
                'DAMAGE_PROPERTY',
                'DAMAGE_CROPS',
                'MAGNITUDE',
                'MAGNITUDE_TYPE',
                'TOR_F_SCALE',
                'TOR_LENGTH',
                'TOR_WIDTH',
                'BEGIN_LAT',
                'BEGIN_LON',
                'EPISODE_NARRATIVE'
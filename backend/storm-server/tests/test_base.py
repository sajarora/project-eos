"""
Project: flask-rest
Author: Saj Arora
Description: Inherited by all tests
"""
import os
import sys

os.environ["FLASK_CONF"] = "TEST"
sys.path.insert(0, '../pylibs')

from unittest import TestCase
import json
from google.appengine.ext import testbed, ndb

from main import app


class TestBase(TestCase):
    base_url = 'api/v1'

    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_app_identity_stub()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_taskqueue_stub()
        self.testbed.init_urlfetch_stub()
        self.client = app.test_client()
        ndb.get_context().clear_cache()

    def tearDown(self):
        self.testbed.deactivate()

    def get(self, endpoint, params=None):
        params = params or dict()
        return self.client.get(self.base_url + endpoint,
                               data=params, follow_redirects=True)

    def post(self, endpoint, data=None):
        return self.client.post(self.base_url + endpoint,
                                data=json.dumps(data),
                                content_type='application/json',
                                follow_redirects=True)

    def delete(self, endpoint):
        return self.client.delete(self.base_url + endpoint,
                                  content_type='application/json',
                                  follow_redirects=True)

    def put(self, endpoint, data=None):
        return self.client.put(self.base_url + endpoint,
                               data=json.dumps(data),
                               content_type='application/json',
                               follow_redirects=True)

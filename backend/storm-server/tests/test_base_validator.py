"""
Project: flask-rest
Author: Saj Arora
Description: 
"""
from api.v1.fundamentals import NamingConvention
from tests import TestBase


class TestNamingConvention(TestBase):
    def testProcess(self):
        name, dest = NamingConvention.process('first_name', NamingConvention.SNAKE_CASE, NamingConvention.SNAKE_CASE)
        self.assertEqual(name, 'first_name')
        self.assertIsNone(dest)

        name, dest = NamingConvention.process('first_name', NamingConvention.SNAKE_CASE, NamingConvention.CAPS)
        self.assertEqual(name, 'FirstName')
        self.assertEqual(dest, 'first_name')

        name, dest = NamingConvention.process('first_name', NamingConvention.SNAKE_CASE, NamingConvention.CAMEL_CASE)
        self.assertEqual(name, 'firstName')
        self.assertEqual(dest, 'first_name')

        name, dest = NamingConvention.process('FirstName', NamingConvention.CAPS, NamingConvention.SNAKE_CASE)
        self.assertEqual(name, 'first_name')
        self.assertEqual(dest, 'FirstName')

        name, dest = NamingConvention.process('FirstName', NamingConvention.CAPS, NamingConvention.CAMEL_CASE)
        self.assertEqual(name, 'firstName')
        self.assertEqual(dest, 'FirstName')

        name, dest = NamingConvention.process('firstName', NamingConvention.CAMEL_CASE, NamingConvention.CAPS)
        self.assertEqual(name, 'FirstName')
        self.assertEqual(dest, 'firstName')

        name, dest = NamingConvention.process('firstName', NamingConvention.CAMEL_CASE, NamingConvention.SNAKE_CASE)
        self.assertEqual(name, 'first_name')
        self.assertEqual(dest, 'firstName')

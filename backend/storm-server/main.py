"""
Project: flask-rest
Author: Saj Arora
Description: Entry point for flask-rest app
"""
from api import SageRest
import config

# start the app
from api.v1 import SageString, SageDateTime, SageResource, SageInteger, SageBool, SageGravatar

app = SageRest.start(__name__, config)

from modules import auth_module, account_module, storm_module, parse_events, population_module

app.set_auth(auth_module) # enables authentication
app.add_modules([account_module, storm_module(), parse_events, population_module()])


@app.after_request
def add_headers(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response
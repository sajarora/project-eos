'use strict';

/**
 * @ngdoc overview
 * @name frontendApp
 * @description
 * # frontendApp
 *
 * Main module of the application.
 */
angular
  .module('frontendApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'angular.morris-chart',
    'ui.bootstrap',
    'angularMoment',
    'uiGmapgoogle-maps',
    'angular-nicescroll',
    'rzModule'
  ])
  .config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyCmbMdYBOYoQbeyIYOVpJNGyGUAY3g23e8',
      v: '3.20', //defaults to latest 3.X anyhow
      libraries: 'weather,geometry,visualization'
    });
  })
  .config(function ($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider

    // HOME STATES AND NESTED VIEWS ========================================
      .state('home', {
        abstract: true,
        url: '/home',
        controller: 'MainCtrl',
        template: '<div class="container-fluid mimin-wrapper">' +
        '<div ui-view="left-menu"></div>' +
        '<div ui-view="content"></div>' +
        '</div>'
      })
      .state('home.welcome', {
        url: '',
        views: {
          'left-menu': {
            templateUrl: '../views/left-menu.html',
            controller: 'SidebarCtrl'
          },
          'content': {
            templateUrl: '../views/main.html',
            controller: 'WelcomeCtrl'
          }
        }
      });
  });

'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:WelcomeCtrl
 * @description
 * # WelcomeCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('WelcomeCtrl', function ($log, $scope, BackendService, uiGmapGoogleMapApi) {
    $scope.slider = {
      value: 2005,
      options: {
        floor: 2005,
        ceil: 2016
      }
    };

    $scope.storms = {};
    $scope.markers = {};
    //$scope.resourceAnalysis = {};
    for (var i = $scope.slider.options.floor; i <= $scope.slider.options.ceil; i++){
      $scope.storms[i] = [];
      $scope.markers[i] = {
        data: [],
        total: 0,
        loaded: 0,
        isLoaded: false
      };
      //$scope.resourceAnalysis[i] = [];
    }


    var updateMarkers = function(year, storms){
      if (!$scope.storms)
        return;

      for(var i = 0; i < storms.length; i++ ) {
        var storm = storms[i];
        $scope.storms[year].push(storm);
        if (storm.latitude && storm.longitude) {
          var newMarker = {
            id: storm.name,
            latitude: parseFloat(storm.latitude),
            longitude: parseFloat(storm.longitude),
            showWindow: false,
            title: "Marker" + i
          };

          newMarker.onClicked = function (sender) {
            alert(sender);
          };

          $scope.markers[year].data.push(newMarker);
        }
      }
      $scope.markers[year].loaded += storms.length;
    };

    var updateAnalytics = function(year) {
      $scope.resourceAnalysis = [];
      var storms = $scope.storms[year];
      angular.forEach(storms, function (storm) {
        if (storm.latitude && storm.longitude) {
          var damageIndex = parseInt(storm.direct_injuries) * 2 + parseInt(storm.indirect_injuries) * 2 +
            parseInt(storm.direct_deaths) * 5 + parseInt(storm.indirect_deaths) * 5 +
            parseFloat(storm.property_damage) * 1.5 + parseFloat(storm.crops_damage) * 2;
          $scope.resourceAnalysis.push({
            y: damageIndex,
            a: parseFloat(storm.latitude),
            b: parseFloat(storm.longitude)
          })
        }
      });
    };

    var updateFromDb = function(year, cursor){
      if ($scope.markers[year].isLoaded) //already loaded
        return;

      BackendService.list({
        filter:('year=' + year),
        limit:300,
        cursor: cursor
      }).then(function(response){
        if (response.data) {
          updateMarkers(year, response.data.list || []);
          $scope.markers[year].total = response.data.meta.total_count;
          if (response.data.meta && response.data.meta.more) {
            //console.log('there is more...markers');
            updateAnalytics(year); // TODO remove from here
            updateFromDb(year, response.data.meta.next_cursor);
          } else {
            $scope.markers[year].isLoaded = true;
            updateAnalytics(year)
          }
        }
      }, function(error){
        console.log(error);
      });
    };


    updateFromDb($scope.slider.value);

    $scope.$on("slideEnded", function() {
      updateFromDb($scope.slider.value);
    });

    // uiGmapGoogleMapApi is a promise.
    // The "then" callback function provides the google.maps object.
    uiGmapGoogleMapApi.then(function(maps) {
      $scope.map = { center: { latitude: 41.850033, longitude: -87.6500523 }, zoom: 4 };
    });

  });

'use strict';

/**
 * @ngdoc service
 * @name frontendApp.BackendService
 * @description
 * # BackendService
 * Service in the frontendApp.
 */
angular.module('frontendApp')
  .service('BackendService', function ($http, $interval, $log) {
    var baseUrl = 'https://eos-project.appspot.com/api/v1/';
    var service = this;
    service.list = function(params){
      return $http({
        url: baseUrl + 'storms',
        params: params,
        method: 'get'
      });
    }
  });

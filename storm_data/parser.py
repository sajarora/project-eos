"""
Project: storm_data
Author: Saj Arora
Description: 
"""
import os

from csv_reader import CsvReader
from csv_writer import CsvWriter
from storm_file_type import StormFileType

data_dir = os.path.join(os.path.dirname(__file__), 'data')
output_dir = os.path.join(os.path.dirname(__file__), 'output')


def load_all_csv_in_dir(file_type):
    directory = data_dir + '/' + file_type
    for root, dirs, files in os.walk(directory):
        for file in files:
            data = []
            if file.endswith(".csv"):
                # instantiate csv reader class
                file_path = directory + '/' + file
                reader = CsvReader(file_type, file_path)
                data = reader.get_data()
                print 'Finished parsing %s' % file
                print 'Total: %d' % len(data)
                print '---------------------------'
                CsvWriter(data, output_dir + '/' + file)


def parse_storm_details():
    load_all_csv_in_dir(StormFileType.DETAILS)

if __name__ == '__main__':
    parse_storm_details()
"""
Project: storm_data
Author: Saj Arora
Description: 
"""
import csv
import logging

import re

from storm_file_type import StormFileType


class CsvReader(object):
    data = []

    def __init__(self, file_type, filename):
        self.file_type = file_type
        self.filename = filename
        # extract data
        logging.info("parsing file: " + filename)

    def get_data(self):
        all_storms = self._parse_data(self.filename)
        return self._extract_features(all_storms)

    def _parse_data(self, f):
        # auto closes open file
        all_storms = []
        with open(f, 'rb') as csvfile:
            try:
                reader = csv.reader(csvfile, delimiter=',', quotechar='"')
                for number, row in enumerate(reader):
                    if number == 0: #skip header row
                            continue

                    storm_dict = dict()
                    for index, item in enumerate(row):
                        if index < len(StormFileType.data_csv_headers.get(self.file_type)):
                            storm_dict[
                                StormFileType.data_csv_headers.get(self.file_type)[index]
                            ] = re.sub('"', "", item).strip()

                    all_storms.append(storm_dict)
            except Exception as e:
                logging.error(e.message)

        return all_storms

    def _extract_features(self, all_storms):
        data = []
        for storm in all_storms:
            storm_dict = dict() # final storm data dict
            for name, value in StormFileType.data_dict.get(self.file_type).iteritems(): # extract only the wanted attributes
                if hasattr(value, '__call__'): # function
                    storm_dict[name] = value(storm)
                elif value in storm:
                    storm_dict[name] = storm.get(value)
            data.append(storm_dict)
        return data
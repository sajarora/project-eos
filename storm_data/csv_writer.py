"""
Project: flask-rest
Author: Saj Arora
Description: 
"""
import csv


class CsvWriter(object):
    def __init__(self, data, filename):
        self.filename = filename
        self._write_data(data)

    def _write_data(self, data):
        #local use only
        with open(self.filename, 'wb') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='"',
                                    quoting=csv.QUOTE_MINIMAL)

            for index, item in enumerate(data):
                if index == 0: # output the header
                    writer.writerow([x for x in item])

                writer.writerow([v for k, v in item.iteritems()])